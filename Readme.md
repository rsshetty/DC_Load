Carga electrónica (DC Load)
===========================

Esta carga electrónica la he diseñado con la intención de aprender un poco más sobre amplificadores operacionales, electrónica de "potencia", mejorar el método de
realización de PCBs casero que tenía hasta ahora y una vez acabada, añadirla a mi mesa
de trabajo como una nueva herramienta que me permita poder probar baterías
y fuentes de alimentación.


![DC_Load](https://2.bp.blogspot.com/-EAFoPGEJpvo/WyokwZnH6kI/AAAAAAAAAbM/IBo83Excuuss3GMJYd8XHGg6TBSgGcs0wCKgBGAs/s640/20171028_0139.jpg)


Las especificaciones finales son las siguientes:

- Potencia máxima: 300 W.
- Amperaje máximo: 15 A.
- Voltaje máximo: 100V.
- Modo de corriente constante.
- Modo de potencia constante.
- Modo de resistencia constante.
- Modo de transición. (Cambio entre dos valores de carga cada cierto tiempo).
- Medición de la resistencia interna de una batería.
- Medición de la capacidad de una batería en Ah.
- Error de lectura en tensión de ± 5 mV aprox.
- Error de lectura en corriente de ± 4 mA aprox.
- Protección por límite de corriente.
- Protección por límite de potencia.
- Protección por límite de voltaje.
- Protección por límite de temperatura.
- Control automático de los ventiladores.
- Control de temperatura y circuito de control de corriente independiente para cada mosfet.
- Capacidad de desconectar la carga físicamente (relés), para protección contra polaridad- inversa y cuando se rebasa cualquier límite (temperatura, voltaje, potencia o intensidad).
- Fusibles en la salida en caso de fallo en la electrónica.
- Lectura de voltaje independiente (4 Wire).

Al igual que el proyecto anterior, todos los programas y librerías utilizados son gratuitos y la mayoría opensource.

Todos los ficheros utilizados: esquemáticos, serigrafía, firmware, PCB, etc..., se encuentran disponibles en este repositorio.

Para obtener más información de este proyecto consultar el [blog.](https://www.circuiteando.net/2018/05/carga-electronica-parte-1-introduccion.html)